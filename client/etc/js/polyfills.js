(function () {
	"use strict";

	module.exports = {
		"find": function(predicate) {
			if (this === null) {
				throw new TypeError("Array.prototype.find called on null or undefined");
			}
			if (typeof predicate !== "function") {
				throw new TypeError("predicate must be a function");
			}
			var list = Object(this);
			var length = list.length >>> 0;
			var thisArg = arguments[1];
			var value;

			for (var i = 0; i < length; i++) {
				value = list[i];
				if (predicate.call(thisArg, value, i, list)) {
					return value;
				}
			}
			return undefined;
		},
		"findIndex": function(predicate) {
			if (this === null) {
				throw new TypeError('Array.prototype.findIndex called on null or undefined');
			}
			if (typeof predicate !== 'function') {
				throw new TypeError('predicate must be a function');
			}
			var list = Object(this);
			var length = list.length >>> 0;
			var thisArg = arguments[1];
			var value;

			for (var i = 0; i < length; i++) {
				value = list[i];
				if (predicate.call(thisArg, value, i, list)) {
					return i;
				}
			}
			return -1;
		},
		"some": function (
			a, // expression to test each element of the array against
			b, // optionally change the 'this' context for the given callback
			c, // placeholder iterator variable
			d // placeholder variable (stores context of original array)
		) {
			for (c = 0, d = this; c < d.length; c++) // iterate over all of the array elements
				if (a.call(b, d[c], c, d)) // call the given expression, passing in context, value, index, and original array
					return !0; // if any expression evaluates true, immediately return since 'some' is true
			return !1 // otherwise return false since all callbacks evaluated to false
		}
	}

}());