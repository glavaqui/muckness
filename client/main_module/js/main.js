(function () {
	"use strict";
	const Vue = require("vue");
	const vuex = require("vuex");
	const vueRouter = require("vue-router");

	Vue.use(vuex);
	Vue.use(vueRouter);

	Vue.directive("focus", {
		inserted(el) {
			el.focus();
		}
	});

	return new Vue({
		"el": "#muckness-app",
		"name": "main",
		// "store": require("./store/store"),
		"router": require("./router/index"),
		"render": function (h) {
			return h(require("./components/app.vue"));
		}
	});
}()); 