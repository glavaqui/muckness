"use strict";

module.exports = [{
	"name": "Home",
	"path": "/",
	"default": true,
	"component": require("../../components/app.vue"),
	"meta": {
		"scrollEl": null,
		"scrollPos": null
	}
}];
