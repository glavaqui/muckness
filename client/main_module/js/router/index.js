(function () {
	"use strict";


	const VueRouter = require("vue-router");

	module.exports = new VueRouter({
		"base": "/",
		"mode": "history",
		"routes": [{
			"name": "Home",
			"path": "/",
			"default": true,
			"component": require("../components/app.vue"),
			"meta": {
				"scrollEl": null,
				"scrollPos": null
			}
		}]
	});
}());