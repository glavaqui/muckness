(function () {
	"use strict";

	if (!Array.prototype.find) { Array.prototype.find = require("../../etc/js/polyfills").find; }
	if (!Array.prototype.some) { Array.prototype.find = require("../../etc/js/polyfills").some; }
	if (!Array.prototype.findIndex) { Array.prototype.findIndex = require("../../etc/js/polyfills").findIndex; }
	if (!window.Promise) { window.Promise = require("promise-polyfill"); }

	const Clipboard = require("clipboard");
	return new Clipboard(".clip");

}());