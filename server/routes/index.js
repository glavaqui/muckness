(function () {
	"use strict";

	// const handleError = require("../helpers/errorHandler").handleError; @TODO use to handle express errors

	module.exports = function (app) {

		app.get(["/", "/app", "/app/", "/app/*"],
			(req, res, next) => {
				req.session.originalUrl = req.originalUrl;
				next();
			},
			(req, res) => res.status(200).render("./main_module/index.html")
		);
	};

}());