#!/bin/bash
# Push app
cf push "$CF_APP" --no-start
cf set-env "$CF_APP" APP_SECRET "$APP_SECRET"
cf start "$CF_APP"
cf logs "${CF_APP}" --recent