#!/bin/bash
export NVM_DIR=/home/pipeline/nvm
export NODE_VERSION=12.15.0
export NPM_VERSION=6.13.4
export NVM_VERSION=0.35.2


npm config delete prefix \
  && curl https://raw.githubusercontent.com/creationix/nvm/v${NVM_VERSION}/install.sh | sh \
  && . $HOME/.nvm/nvm.sh \
  && nvm install $NODE_VERSION \
  && nvm alias default $NODE_VERSION \
  && nvm use $NODE_VERSION \
  && node -v \
  && npm -v

npm install gulp -g
npm install
gulp set-manifest
gulp build-all
gulp generate-sw
gulp generate-logs-sw